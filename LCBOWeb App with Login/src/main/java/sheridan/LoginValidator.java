package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {

        if (loginName == null) {
            return false;
        }
          
        loginName = loginName.trim();
        
     return loginName.matches("[A-Za-z0-9]+") && loginName.length() >= 6;
        
	}
}