package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Password123" ) );
	}
	
	@Test
	public void testIsValidLoginException() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName(null));
	}
	
	@Test
	public void testIsValidLoginExceptionSpaces() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName(" 		"));
	}
	

	@Test
	public void testIsOnlyAlphaNumericRegular() {
		assertTrue("Login Name can only contain AplhaNumeric characters", LoginValidator.isValidLoginName("Password123"));
	}
	
	@Test
	public void testIsOnlyAlphaNumericException() {
		assertFalse("Login Name can only contain AplhaNumeric characters", LoginValidator.isValidLoginName("Password$"));
	}
	
	@Test
	public void testIsValidLengthRegular() {
		assertTrue("Login Name must contain atleast 6 characters or numbers", LoginValidator.isValidLoginName("Password123"));
	}
	
	@Test
	public void testIsValidLengthException() {
		assertFalse("Login Name must contain atleast 6 characters or numbers", LoginValidator.isValidLoginName("Pass2"));
	}

}
